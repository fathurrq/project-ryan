import { useTheme } from '@emotion/react';
import { Grid, Typography, useMediaQuery } from '@mui/material';
import { Box } from '@mui/system';

import Ilustration from 'components/auth/login/Ilustration';
import LoginForm from 'components/auth/login/LoginForm';
import RegisterNow from 'components/auth/login/RegisterNow';
import Welcome from 'components/auth/login/Welcome';
import AuthCardWrapper from 'components/authentication/AuthCardWrapper';
import Page from 'components/ui-component/Page';
import LAYOUT from 'constant';
import { useFormik } from 'formik';
import Layout from 'layout';
import React from 'react';
import { loginValidationSchema } from 'schema/auth/login';

const LoginPageContent = () => {
  const theme = useTheme();
  const matchDownSM = useMediaQuery(theme.breakpoints.down('md'));
  const formik = useFormik({
    initialValues: {
      email: '',
      password: ''
    },
    validationSchema: loginValidationSchema,
    onSubmit: (value) => {
      console.log(value);
    }
  });
  return (
    <Page title="Login">
      <Grid item container alignItems="center" sx={{ minHeight: '100vh', width: '100vw' }}>
        <Ilustration />
        <Grid item container justifyContent="center" md={6} lg={6} sx={{ my: 3 }}>
          <AuthCardWrapper>
            <Grid container spacing={2} justifyContent="center">
              <Welcome matchDownSM={matchDownSM} theme={theme} />
              <Grid item xs={12}>
                <Box sx={{ width: '100%', bgcolor: 'error.light', padding: '15px 23px' }}>
                  <Typography color="error.main" variant="body1">
                    Password atau Email Salah. Silahkan Ulangi Kembali.
                  </Typography>
                </Box>
              </Grid>
              <LoginForm formik={formik} />
              <RegisterNow />
            </Grid>
          </AuthCardWrapper>
        </Grid>
      </Grid>
    </Page>
  );
};

LoginPageContent.getLayout = function getLayout(page) {
  return <Layout variant={LAYOUT.minimal}>{page}</Layout>;
};

export default LoginPageContent;
