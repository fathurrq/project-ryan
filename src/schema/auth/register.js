import * as yup from 'yup';

export const registerValidationScehma = yup.object({
  name: yup.string().required('Name is required'),
  email: yup.string().email('Enter a valid email').required('Email is required'),
  phoneNumber: yup
    .string()
    .required()
    .matches(/^[0-9]+$/, 'Must be only digits')
    .min(7, 'Must be minimal 10 digits!')
    .max(9, 'Must be maximal 12 digits'),
  password: yup.string().min(8, 'Password should be of minimum 8 characters length').required('Password is required'),
  passwordConfirmation: yup.string().oneOf([yup.ref('password'), null], 'Passwords must match')
});
