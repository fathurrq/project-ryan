import styled from '@emotion/styled';
import { Button, Grid, ThemeProvider, Typography, createTheme } from '@mui/material';
import Image from 'next/image';

const SectionHeroWrapper = styled.div`
  padding: 48px;
`;

const theme = createTheme({
  typography: {
    sectionTitleVariant: {
      fontSize: 30,
      lineHeight: 1.5,
      fontWeight: 700
    },
    sectionDescVariant: {
      fontSize: 18,
      lineHeight: '30px'
    },
    sectionHeadingVariant: {
      fontSize: 60,
      fontWeight: 700,
      lineHeight: '45px',
      color: 'rgba(88, 188, 255, 1)'
    }
  }
});

const SectionHero = () => (
  <ThemeProvider theme={theme}>
    <SectionHeroWrapper>
      <Grid container style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Grid item sx={12} md={6}>
          <Typography component="h2" variant="sectionTitleVariant" marginBottom={2}>
            Platform Pengiriman
            <Typography component="span" color="#2EABFF" variant="sectionTitleVariant">
              {' '}
              No.1{' '}
            </Typography>
            untuk Kirim Barang, Mudah dan Terjangkau
          </Typography>
          <Typography variant="sectionDescVariant">
            Dapatkan diskon ongkir <strong>mulai dari 25%</strong> dan <strong>Cashback 50%!!</strong> Pelajari lebih lanjut untuk tawaran menarik lainnya
          </Typography>
          <Grid container mt={6}>
            <Grid item sx={4}>
              <Typography variant="sectionHeadingVariant">200+</Typography>
            </Grid>
            <Grid item sx={8} ml={3}>
              <Typography variant="sectionDescVariant">
                Pebisnis online sudah bergabung <br /> dengan{' '}
                <Typography component="span" color="#2EABFF">
                  <strong>Completed</strong>
                </Typography>
              </Typography>
            </Grid>
          </Grid>
          <Button variant="contained" sx={{ px: 5, py: 2, mt: 3, backgroundColor: '#2EABFF' }}>
            Gabung Sekarang
          </Button>
        </Grid>
        <Grid item sx={12} md={6}>
          <Image
            src="/assets/images/landingpage/hero/hero.png"
            alt="Hero Banner"
            width={0}
            height={0}
            sizes="100vw"
            style={{ width: '100%', height: 'auto' }}
          />
        </Grid>
      </Grid>
    </SectionHeroWrapper>
  </ThemeProvider>
);

export default SectionHero;
