// material-ui
import { useTheme } from '@mui/material/styles';
import { Button, CardMedia, Container, Grid, Stack, Typography } from '@mui/material';

// eslint-disable-next-line import/no-named-as-default-member
import JanganKhawatir from 'components/testcomponent/JanganKhawatir';
import Feature from 'components/testcomponent/Feature';
import KenapaCompleted from 'components/testcomponent/KenapaCompleted';

// ==============================|| LANDING - CUSTOMIZE ||============================== //

const CustomizeSection = () => {
  const theme = useTheme();
  const listSX = {
    display: 'flex',
    alignItems: 'center',
    gap: '0.7rem',
    padding: '10px 0',
    fontSize: '1rem',
    color: theme.palette.grey[900],
    svg: { color: theme.palette.secondary.main }
  };

  return (
    <Container
      sx={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
      }}
    >
      <Grid container justifyContent="space-between" alignItems="center" spacing={{ xs: 3, sm: 5, md: 6, lg: 10 }}>
        <KenapaCompleted />
        <JanganKhawatir />
        <Feature />
      </Grid>
    </Container>
  );
};

export default CustomizeSection;
