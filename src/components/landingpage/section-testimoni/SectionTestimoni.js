import styled from '@emotion/styled';
import SectionTitle from './SectionTitle';
import VideoTestimoni from './VideoTestimoni';
import CardTestimoni from './CardTestimoni';
import testimonyData from '../../../data/dummy/testimoni/index.json';
import { Carousel } from 'react-responsive-carousel';
import 'react-responsive-carousel/lib/styles/carousel.min.css';

const SectionTestimoniWrapper = styled.div`
  padding: 120px 0;
`;

const SectionTestimoni = () => (
  <SectionTestimoniWrapper>
    <SectionTitle />
    <Carousel autoPlay="true" infiniteLoop="true" swipeable="true" selectedItem={3}>
      {testimonyData.map((t) => (
        <CardTestimoni data={t} />
      ))}
    </Carousel>
    <VideoTestimoni />
  </SectionTestimoniWrapper>
);

export default SectionTestimoni;
