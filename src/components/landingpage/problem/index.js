// pages/index.js

import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import banyakPaketBermasalah from '../../../public/assets/images/landing/problems/banyak-paket-bermasalah.svg';
import cashbackRendah from '../../../public/assets/images/landing/problems/cashback-rendah.svg';
import ongkirRtsMahal from '../../../public/assets/images/landing/problems/ongkir-rts-mahal.svg';
import prosesManual from '../../../public/assets/images/landing/problems/proses-manual.svg';
import rtsYangTinggi from '../../../public/assets/images/landing/problems/rts-yang-tinggi.svg';

const useStyles = makeStyles((theme) => ({
  root: {
    fontFamily: 'inter, sans-serif',
    flexGrow: 1,
    padding: theme.spacing(2),
    [theme.breakpoints.up('sm')]: {
      padding: '82px'
    },
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  title: {
    textAlign: 'center',
    marginBottom: '80px'
  },
  paper: {
    display: 'flex',
    alignItems: 'center',
    height: '160px',
    textAlign: 'center',
    padding: theme.spacing(2),
    border: 'none',
    boxShadow: 'none',
    width: '430px'
  },
  imageContainer: {
    marginRight: theme.spacing(2),
    borderRadius: '5px',
    overflow: 'hidden',
    width: '88px',
    height: '88px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  image: {
    maxWidth: '100%',
    maxHeight: '100%',
    objectFit: 'cover'
  },
  explanation: {
    flex: 1,
    marginLeft: theme.spacing(2),
    textAlign: 'left',
    height: '160px'
  }
}));

const HomePage = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <h1 className={classes.title}>Problems</h1>

      {/* Grid Container */}
      <Grid container spacing={9}>
        <Grid item xs={12} sm={12} md={6} lg={4}>
          <Paper className={classes.paper}>
            <div className={classes.imageContainer} style={{ backgroundColor: '#F1FCFF' }}>
              <img className={classes.image} src={banyakPaketBermasalah.src} alt="banyak paket bermasalah" />
            </div>
            <div className={classes.explanation}>
              <h2>Banyak Paket Bermasalah</h2>
              <p style={{ width: '252px' }}>
                Kamu sering mengalami banyak kendala dalam melakukan pengiriman seperti Paket Hilang, Rusak atau Paket dicuri?
              </p>
            </div>
          </Paper>
        </Grid>

        <Grid item xs={12} sm={12} md={6} lg={4}>
          <Paper className={classes.paper}>
            <div className={classes.imageContainer} style={{ backgroundColor: '#FFE6E3' }}>
              <img className={classes.image} src={ongkirRtsMahal.src} alt="Ongkir RTS mahal" />
            </div>
            <div className={classes.explanation}>
              <h2>Ongkir RTS Mahal</h2>
              <p style={{ width: '252px' }}>
                Seringkali Bisnis Owner mengalami masalah dalam membayar Ongkir RTS yang mahal ke Ekspedisi dan Platform
              </p>
            </div>
          </Paper>
        </Grid>

        <Grid item xs={12} sm={12} md={6} lg={4}>
          <Paper className={classes.paper}>
            <div className={classes.imageContainer} style={{ backgroundColor: '#FFEED9' }}>
              <img className={classes.image} src={rtsYangTinggi.src} alt="RTS yang tinggi" />
            </div>
            <div className={classes.explanation}>
              <h2>RTS yang Tinggi</h2>
              <p style={{ width: '252px' }}>
                RTS yang tinggi disebabkan oleh banyak faktor eksternal, bisa jadi Ekspedisi yang kurang kompeten, Customer yang menolak
                membeli, dan banyak lagi.
              </p>
            </div>
          </Paper>
        </Grid>

        <Grid item xs={12} sm={12} md={6} lg={4}>
          <Paper className={classes.paper}>
            <div className={classes.imageContainer} style={{ backgroundColor: '#F3DEFF' }}>
              <img className={classes.image} src={prosesManual.src} alt="Proses Manual" />
            </div>
            <div className={classes.explanation}>
              <h2>Proses Manual</h2>
              <p style={{ width: '252px' }}>Dalam memanage semua paket pengiriman kamu, kamu masih menggunakan cara yang manual?</p>
            </div>
          </Paper>
        </Grid>

        <Grid item xs={12} sm={12} md={6} lg={4}>
          <Paper className={classes.paper}>
            <div className={classes.imageContainer} style={{ backgroundColor: '#F0FFFE' }}>
              <img className={classes.image} src={cashbackRendah.src} alt="cashback rendah" />
            </div>
            <div className={classes.explanation}>
              <h2>Cashback Rendah</h2>
              <p style={{ width: '252px' }}>Dalam mengirim volume paket setiap bulannya kamu mendapatkan cashback yang Rendah?</p>
            </div>
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
};

export default HomePage;
