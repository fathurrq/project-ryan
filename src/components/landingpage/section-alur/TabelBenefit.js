import styled from '@emotion/styled';

const TableBenefitWrapper = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 120px;
`;

const Table = styled.div`
  border-collapse: separate;
  border-spacing: 0;
`;

const TableBenefitRow = styled.div`
  display: flex;
  margin-bottom: 16px;
  height: 110px;
  &:nth-child(1) {
    height: 80px;
    background-color: rgba(46, 171, 255, 1);
  }
`;

const TableBenefitHeading = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 210px;
  margin-right: 16px;
  background-color: rgba(0, 104, 174, 1);
  color: #ffffff;
  font-size: 20px;
  font-weight: 600;
  line-height: 30px;
  text-align: center;
  border-radius: 4px;
  @media (max-width: 576px) {
    width: 70px;
    font-size: 16px;
  }
`;

const TableBenefitSubHeading = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 240px;
  text-align: center;
  background-color: rgba(46, 171, 255, 1);
  font-size: 20px;
  font-weight: 600;
  color: #ffffff;
  @media (max-width: 576px) {
    width: 80px;
    font-size: 16px;
  }
`;

const TableBenefitItem = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 240px;
  padding: 24px;
  margin-top 16px;
  text-align: center;
  font-size: 18px;
  line-height: 30px;
  background-color: rgba(248, 248, 248, 1);
  @media (max-width: 576px) {
    width: 80px;
    font-size: 14px;
  }
`;

const TableBenefit = () => (
  <TableBenefitWrapper>
    <Table>
      <TableBenefitRow>
        <TableBenefitHeading>Benefit</TableBenefitHeading>
        <TableBenefitSubHeading>Ninja Express</TableBenefitSubHeading>
        <TableBenefitSubHeading>Sicepat</TableBenefitSubHeading>
        <TableBenefitSubHeading>JNE</TableBenefitSubHeading>
        <TableBenefitSubHeading>JNT</TableBenefitSubHeading>
      </TableBenefitRow>
      <TableBenefitRow>
        <TableBenefitHeading>Biaya COD</TableBenefitHeading>
        <TableBenefitItem>
          2.5% <br /> (PPN 11%)
        </TableBenefitItem>
        <TableBenefitItem>
          <em>Coming Soon</em>
        </TableBenefitItem>
        <TableBenefitItem>
          <em>Coming Soon</em>
        </TableBenefitItem>
        <TableBenefitItem>
          <em>Coming Soon</em>
        </TableBenefitItem>
      </TableBenefitRow>
      <TableBenefitRow>
        <TableBenefitHeading>Biaya Retur</TableBenefitHeading>
        <TableBenefitItem>Gratis se-Indonesia</TableBenefitItem>
        <TableBenefitItem>
          <em>Coming Soon</em>
        </TableBenefitItem>
        <TableBenefitItem>
          <em>Coming Soon</em>
        </TableBenefitItem>
        <TableBenefitItem>
          <em>Coming Soon</em>
        </TableBenefitItem>
      </TableBenefitRow>
      <TableBenefitRow>
        <TableBenefitHeading>Cashback</TableBenefitHeading>
        <TableBenefitItem>50%</TableBenefitItem>
        <TableBenefitItem>
          <em>Coming Soon</em>
        </TableBenefitItem>
        <TableBenefitItem>
          <em>Coming Soon</em>
        </TableBenefitItem>
        <TableBenefitItem>
          <em>Coming Soon</em>
        </TableBenefitItem>
      </TableBenefitRow>
      <TableBenefitRow>
        <TableBenefitHeading>
          Support <br /> Operasional
        </TableBenefitHeading>
        <TableBenefitItem>Tim Implant (CRM) & Data Analysis (S&K)</TableBenefitItem>
        <TableBenefitItem>
          <em>Coming Soon</em>
        </TableBenefitItem>
        <TableBenefitItem>
          <em>Coming Soon</em>
        </TableBenefitItem>
        <TableBenefitItem>
          <em>Coming Soon</em>
        </TableBenefitItem>
      </TableBenefitRow>
    </Table>
  </TableBenefitWrapper>
);

export default TableBenefit;
