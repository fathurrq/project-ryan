import { styled } from '@mui/material';
import { Box } from '@mui/system';
import Image from 'next/image';
import PropTypes from 'prop-types';

const AlurNum = styled('span')({
  fontSize: 40,
  fontWeight: 500,
  lineHeight: 0.75
});

const AlurIconBox = styled('div')({
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  height: 130,
  marginTop: 24,
  boxSizing: 'border-box',
  borderRadius: 20,
  borderWidth: 8,
  borderStyle: 'solid'
});

const AlurIcon = ({ num = '00', title, color, icon }) => (
  <Box>
    <AlurNum sx={{ color }}>{num}</AlurNum>

    <AlurIconBox sx={{ borderColor: color, boxShadow: `0px 4px 20px 0px ${color}` }}>
      <Image src={icon} alt={title} width={0} height={0} sizes="100vw" style={{ width: '55px', height: 'auto' }} />
    </AlurIconBox>
  </Box>
);

AlurIcon.propTypes = {
  num: PropTypes.string,
  title: PropTypes.string,
  color: PropTypes.string,
  icon: PropTypes.string
};

export default AlurIcon;
