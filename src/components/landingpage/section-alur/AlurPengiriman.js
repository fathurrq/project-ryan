import { alurPengiriman } from 'data/dummy/alur-pengiriman';
import AlurItem from './AlurItem';
import styled from '@emotion/styled';
import { addZeroes } from 'utils/commons/addZeroes';

const AlurPengirimanWrapper = styled.div`
  display: flex;
  justify-content: center;
  gap: 72px;
  margin-top: 96px;
  @media (max-width: 576px) {
    gap: 16px;
  }
`;

const AlurPengiriman = () => (
  <AlurPengirimanWrapper>
    {alurPengiriman.map((alurItem, i) => (
      <AlurItem num={addZeroes(i + 1)} text={alurItem.text} color={alurItem.colorTheme} icon={alurItem.icon} />
    ))}
  </AlurPengirimanWrapper>
);

export default AlurPengiriman;
