import SectionTitle from './SectionTitle';
import AlurPengiriman from './AlurPengiriman';
import styled from '@emotion/styled';
import TableBenefit from './TabelBenefit';

const SectionAlurWrapper = styled.div`
  padding: 120px 0;
`;

const SectionAlur = () => (
  <SectionAlurWrapper>
    <SectionTitle />
    <AlurPengiriman />
    <TableBenefit />
  </SectionAlurWrapper>
);

export default SectionAlur;
