import styled from "@emotion/styled";

const SectionFiturWrapper = styled.div`
`;

const SectionFitur = () => (
  <SectionFiturWrapper>
    <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', gap: 32, height: 200, width: '100%', borderRadius: '75%', backgroundColor: 'rgba(245, 251, 255, 1)' }}>
      <img src="/assets/images/landingpage/fitur/fitur-logo.png"  width={180} height={44}/>
      <p style={{ fontSize: 22, lineHeight: '32px' }}>
        hadir sebagai solusi untuk memudahkan pengiriman kamu <br/> dengan berbagai benefit tanpa minimum jumlah pengiriman
      </p>
    </div>

    <div style={{ display: 'flex', justifyContent: 'center', flexWrap: 'wrap', columnGap: 32, marginTop: '-96px', rowGap: 64, padding: '172px 0 300px', backgroundColor: 'rgba(245, 251, 255, 1)', backgroundImage: 'url(assets/images/landingpage/fitur/fitur-bg.png)', backgroundPosition: '40% 0' }}>
      <div style={{ backgroundColor: '#ffffff', display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', width: 352, height: 325, padding: '0 48px', borderRadius: 16, boxShadow: '0px 2px 25px 0px rgba(25, 91, 106, 0.4)'}}>
        <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width: 105, height: 105, background: 'rgba(241, 252, 255, 1)', borderRadius: '100%'}}>
        <img src="/assets/images/landingpage/fitur/fitur-01.png" width={54} height={54}/>
        </div>
        <p style={{ fontSize: 22, fontWeight: 600, color: 'rgba(40, 52, 64, 1)' }}>Cashback sampai 50%</p>
        <p style={{ fontSize: 14, color: 'rgba(80, 80, 80, 1)', textAlign: 'center' }}>
        Completed memberikan cashback pengiriman hingga 35%-50%, dan biaya admin hanya 2%
        </p>
      </div>
      <div style={{ backgroundColor: '#ffffff', display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', width: 352, height: 325, padding: '0 48px', borderRadius: 16, boxShadow: '0px 2px 25px 0px rgba(25, 91, 106, 0.4)'}}>
        <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width: 105, height: 105, background: 'rgba(241, 252, 255, 1)', borderRadius: '100%'}}>
        <img src="/assets/images/landingpage/fitur/fitur-02.png"  width={54} height={54}/>
        </div>
        <p style={{ fontSize: 22, fontWeight: 600, color: 'rgba(40, 52, 64, 1)' }}>Pengiriman On Time</p>
        <p style={{ fontSize: 14, color: 'rgba(80, 80, 80, 1)', textAlign: 'center' }}>
        Completed Bermitra dengan
Ekspedisi yang Terintegrasi
dengan baik yang
memungkinkan waktu
pengiriman dilakukan tepat
waktu
        </p>
      </div>
      <div style={{ backgroundColor: '#ffffff', display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', width: 352, height: 325, padding: '0 48px', borderRadius: 16, boxShadow: '0px 2px 25px 0px rgba(25, 91, 106, 0.4)'}}>
        <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width: 105, height: 105, background: 'rgba(241, 252, 255, 1)', borderRadius: '100%'}}>
        <img src="/assets/images/landingpage/fitur/fitur-03.png"  width={54} height={54}/>
        </div>
        <p style={{ fontSize: 22, fontWeight: 600, color: 'rgba(40, 52, 64, 1)' }}>Biaya RTS Gratis</p>
        <p style={{ fontSize: 14, color: 'rgba(80, 80, 80, 1)', textAlign: 'center' }}>
        Khawatir kamu rugi karena retur pelanggan? Tenang, Completed
bebas biaya retur atau
pengembalian COD seluruh
Indonesia. *S&K berlaku
        </p>
      </div>
      <div style={{ backgroundColor: '#ffffff', display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', width: 352, height: 325, padding: '0 48px', borderRadius: 16, boxShadow: '0px 2px 25px 0px rgba(25, 91, 106, 0.4)'}}>
        <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width: 105, height: 105, background: 'rgba(241, 252, 255, 1)', borderRadius: '100%'}}>
        <img src="/assets/images/landingpage/fitur/fitur-04.png"  width={54} height={54}/>
        </div>
        <p style={{ fontSize: 22, fontWeight: 600, color: 'rgba(40, 52, 64, 1)' }}>Gratis Jemput Paket</p>
        <p style={{ fontSize: 14, color: 'rgba(80, 80, 80, 1)', textAlign: 'center' }}>
        Cukup tunggu saja di rumah,
kurir akan mendatangi lokasimu
untuk mengambil paket TANPA
MINIMUM ORDER
        </p>
      </div>
      <div style={{ backgroundColor: '#ffffff', display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', width: 352, height: 325, padding: '0 48px', borderRadius: 16, boxShadow: '0px 2px 25px 0px rgba(25, 91, 106, 0.4)'}}>
        <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width: 105, height: 105, background: 'rgba(241, 252, 255, 1)', borderRadius: '100%'}}>
        <img src="/assets/images/landingpage/fitur/fitur-05.png"  width={54} height={54}/>
        </div>
        <p style={{ fontSize: 22, fontWeight: 600, color: 'rgba(40, 52, 64, 1)' }}>Pencairan Setiap Hari</p>
        <p style={{ fontSize: 14, color: 'rgba(80, 80, 80, 1)', textAlign: 'center' }}>
        Kamu bisa tarik dana CODmu
kapanpun dan dimanapun
bahkan saat libur (weekend) dan
juga malam hari loh! Gak pake
nunggu-nunggu lama lagi.
        </p>
      </div>

      

    </div>
    <div style={{ background: '#ffffff', display: 'flex', flexDirection: 'column', margin: 'auto', justifyContent: 'center', alignItems: 'center', width: 'fit-content', padding: '48px 64px 64px', marginTop: '-148px', borderRadius: '148px', boxShadow: '0px 6px 25px 0px rgba(188, 227, 253, 0.25)'}}>
      <p style={{ fontSize: 18, lineHeight: '28px', fontWeight: 600, textAlign: 'center', marginTop: 0 }}>Kami telah bekerjasama dengan Ekspedisi ternama <br />
yang sudah didukung dengan pengiriman COD dan Non-COD</p>
<img src="/assets/images/landingpage/fitur/fitur-rekan.png" style={{ width: '80%', height: 'auto' }} />
    </div>
  </SectionFiturWrapper>
);

export default SectionFitur;
