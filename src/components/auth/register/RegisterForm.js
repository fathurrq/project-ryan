import { Button, Grid, IconButton, InputAdornment, InputLabel, Stack, TextField, Typography } from '@mui/material';
import Link from 'Link';
import { useFormik } from 'formik';
import React from 'react';
import { registerValidationScehma } from 'schema/auth/register';
import { gridSpacing } from 'store/constant';
import EyeClosed from 'svgs/EyeClosed';

const RegisterForm = () => {
  const formik = useFormik({
    initialValues: {
      email: '',
      password: ''
    },
    validationSchema: registerValidationScehma,
    onSubmit: (value) => {
      console.log(value);
    }
  });

  return (
    <form onSubmit={formik.handleSubmit}>
      <Grid container spacing={gridSpacing}>
        <Grid item xs={12}>
          <InputLabel>Email</InputLabel>
          <TextField
            fullWidth
            id="email"
            name="email"
            value={formik.values.email}
            onChange={formik.handleChange}
            placeholder="Email Address"
            error={formik.touched.email && Boolean(formik.errors.email)}
            helperText={formik.touched.email && formik.errors.email}
          />
        </Grid>
        <Grid item xs={12}>
          <InputLabel>Password</InputLabel>
          <TextField
            fullWidth
            id="password"
            name="password"
            type="password"
            value={formik.values.password}
            onChange={formik.handleChange}
            error={formik.touched.password && Boolean(formik.errors.password)}
            helperText={formik.touched.password && formik.errors.password}
            placeholder=""
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton onClick={() => {}}>
                    <EyeClosed width="23px" height="23px" />
                  </IconButton>
                </InputAdornment>
              )
            }}
          />
        </Grid>
        <Grid item xs={12}>
          <InputLabel>Email</InputLabel>
          <TextField
            fullWidth
            id="email"
            name="email"
            value={formik.values.email}
            onChange={formik.handleChange}
            placeholder="Email Address"
            error={formik.touched.email && Boolean(formik.errors.email)}
            helperText={formik.touched.email && formik.errors.email}
          />
        </Grid>
        <Grid item xs={12}>
          <InputLabel>Email</InputLabel>
          <TextField
            fullWidth
            id="email"
            name="email"
            value={formik.values.email}
            onChange={formik.handleChange}
            placeholder="Email Address"
            error={formik.touched.email && Boolean(formik.errors.email)}
            helperText={formik.touched.email && formik.errors.email}
          />
        </Grid>
        <Grid item xs={12}>
          <InputLabel>Email</InputLabel>
          <TextField
            fullWidth
            id="email"
            name="email"
            value={formik.values.email}
            onChange={formik.handleChange}
            placeholder="Email Address"
            error={formik.touched.email && Boolean(formik.errors.email)}
            helperText={formik.touched.email && formik.errors.email}
          />
        </Grid>
        <Grid item xs={12}>
          <Typography variant="subtitle1" sx={{ textDecoration: 'none' }} component={Link} href="/auth/registration" color="primary.800">
            Lupa Password?
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <Stack direction="row" justifyContent="flex-start">
            <Button type="submit" variant="contained" color="primary" sx={{ padding: '13px 70px', borderRadius: '6px' }}>
              Masuk
            </Button>
          </Stack>
        </Grid>
      </Grid>
    </form>
  );
};

export default RegisterForm;
