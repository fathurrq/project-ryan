import { useTheme } from '@emotion/react';
import { Grid, Typography } from '@mui/material';
import { Box } from '@mui/system';
import Image from 'next/image';
import React, { Fragment } from 'react';

const LogoCompleted = '/assets/completed-image/completed-logo.svg';
const LoginIlus = '/assets/completed-image/auth-page/login.png';

const Ilustration = () => {
  const theme = useTheme();
  return (
    <>
      <Grid
        item
        container
        md={6}
        lg={6}
        sx={{ position: 'relative', alignSelf: 'stretch', display: { xs: 'none', md: 'block' } }}
        bgcolor={theme.palette.primary.light}
      >
        <Grid
          item
          container
          justifyContent="flex-start"
          sx={{ display: 'flex', height: '100%', paddingTop: '40px', margin: '0 auto' }}
          alignItems="center"
        >
          <Grid item xs={12}>
            <Box style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
              <Image src={LogoCompleted} alt="completed-logo" width={180} height={44} />
            </Box>
          </Grid>
          <Grid item xs={12}>
            <Box style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center', textAlign: 'center' }}>
              <Image src={LoginIlus} alt="completed-logo" width={553} height={553} />
              <Typography color={theme.palette.success.dark} variant="h4" fontWeight={600} alignItems="center">
                Dapatkan Semua Kemudahan Pengiriman Logistik Dalam Satu Platform.
              </Typography>
            </Box>
          </Grid>
          <Grid item xs={12}>
            <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center', textAlign: 'center' }}>
              <Typography color="primary.200" variant="h4" fontWeight={400} alignItems="center">
                Copyright 2023 @ Completed
              </Typography>
            </Box>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};

export default Ilustration;
