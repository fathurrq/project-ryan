/* eslint-disable react/prop-types */
import styled from '@emotion/styled';
import { Visibility } from '@mui/icons-material';
import { Button, Grid, IconButton, InputAdornment, InputLabel, Stack, TextField, Typography } from '@mui/material';
import { Box } from '@mui/system';
import Link from 'Link';
import React, { useState } from 'react';
import { gridSpacing } from 'store/constant';
import EyeClosed from 'svgs/EyeClosed';

const StyledButton = styled(Button)(() => ({
  padding: '10px 70px',
  height: '48px',
  fontWeight: 500,
  boxShadow: 'none',
  fontSize: '14px'
}));

const LoginForm = (props) => {
  const { formik } = props;
  const [showPassword, setShowPassword] = useState(false);

  return (
    <Grid item xs={12}>
      <form onSubmit={formik.handleSubmit}>
        <Grid container spacing={gridSpacing}>
          <Grid item xs={12}>
            <Box sx={{ display: 'flex', gap: '13px', flexDirection: 'column' }}>
              <InputLabel>Email</InputLabel>
              <TextField
                fullWidth
                id="email"
                name="email"
                value={formik.values.email}
                onChange={formik.handleChange}
                placeholder="Email Address"
                error={formik.touched.email && Boolean(formik.errors.email)}
                helperText={formik.touched.email && formik.errors.email}
              />
            </Box>
          </Grid>
          <Grid item xs={12}>
            <Box sx={{ display: 'flex', gap: '13px', flexDirection: 'column' }}>
              <InputLabel>Password</InputLabel>
              <TextField
                fullWidth
                id="password"
                name="password"
                type={showPassword ? 'text' : 'password'}
                value={formik.values.password}
                onChange={formik.handleChange}
                error={formik.touched.password && Boolean(formik.errors.password)}
                helperText={formik.touched.password && formik.errors.password}
                placeholder="Enter Your Password"
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton onClick={() => setShowPassword(!showPassword)}>
                        {showPassword ? <Visibility /> : <EyeClosed width="23px" height="23px" />}
                      </IconButton>
                    </InputAdornment>
                  )
                }}
              />
            </Box>
          </Grid>
          <Grid item xs={12}>
            <Typography variant="subtitle1" sx={{ textDecoration: 'none' }} component={Link} href="/auth/registration" color="primary.800">
              Lupa Password?
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Stack direction="row" justifyContent="flex-start">
              <StyledButton type="submit" variant="contained" color="primary">
                Masuk
              </StyledButton>
            </Stack>
          </Grid>
        </Grid>
      </form>
    </Grid>
  );
};

export default LoginForm;
