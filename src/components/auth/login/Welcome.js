/* eslint-disable react/prop-types */
import { Grid, Typography } from '@mui/material';
import { Stack } from '@mui/system';

import React from 'react';

const Welcome = (props) => {
  const { matchDownSM, theme } = props;
  return (
    <Grid item xs={12}>
      <Grid
        container
        direction={matchDownSM ? 'column-reverse' : 'row'}
        alignItems={matchDownSM ? 'center' : 'inherit'}
        justifyContent={matchDownSM ? 'center' : 'space-between'}
      >
        <Grid item>
          <Stack justifyContent={matchDownSM ? 'center' : 'flex-start'} textAlign={matchDownSM ? 'center' : 'inherit'}>
            <Typography color={theme.palette.grey[10]} gutterBottom variant={matchDownSM ? 'h3' : 'h2'}>
              Selamat datang Kembali
            </Typography>
            <Typography color={theme.palette.grey[500]} variant="body1">
              Silahkan masuk dan memulai kemudahan dalam mengelola tokomu dalam satu platform.
            </Typography>
          </Stack>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default Welcome;
