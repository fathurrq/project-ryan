import Link from 'Link';

// material-ui
import { useTheme, styled } from '@mui/material/styles';
import { Alert, Button, Card, CardContent, CardMedia, Grid, InputLabel, List, ListItemButton, ListItemIcon, ListItemText, TextField, Typography } from '@mui/material';
import MuiTypography from '@mui/material/Typography';

import FiberManualRecordIcon from '@mui/icons-material/FiberManualRecord';

// project imports
import LAYOUT from 'constant';
import Layout from 'layout';
import Page from 'components/ui-component/Page';
import { DASHBOARD_PATH } from 'config';
import AnimateButton from 'components/ui-component/extended/AnimateButton';
import { gridSpacing } from 'store/constant';

// assets
import HomeTwoToneIcon from '@mui/icons-material/HomeTwoTone';
import MainCard from 'components/ui-component/cards/MainCard';
import Image from 'next/image';
import { Box } from '@mui/system';

const imagePurple = '/assets/images/maintenance/img-error-purple.svg';

// styles
const CardMediaWrapper = styled('div')({
  maxWidth: 720,
  margin: '0 auto',
  position: 'relative'
});
// ==============================|| ERROR PAGE ||============================== //

const Error = () => {
  const theme = useTheme();

  return (
    <Page title="Verifikasi KTP">
      <MainCard title="Upload Foto KTP">
        <Grid container rowSpacing={4}>
          <Grid item>
            <Alert variant="filled" severity="warning">
              Kamu harus melengkapi data diri kamu dan verifikasi dengan menggunakan KTP
            </Alert>
          </Grid>
          <Grid item>
            <Grid container>
              <Grid item xs={4}>
                <img src="/assets/images/registration/verifikasi-ktp.png" alt="Contoh Gambar KTP" /> 
              </Grid>
              <Grid item xs={8}>
                <MuiTypography variant="h2" gutterBottom>
                  Panduan Foto KTP
                </MuiTypography>

                <List component="div" disablePadding>
                  <ListItemButton sx={{ pl: 0 }}>
                    <ListItemIcon>
                      <FiberManualRecordIcon sx={{ fontSize: '0.5rem' }} />
                    </ListItemIcon>
                    <ListItemText primary="Foto dan data nomor KTP terbaca dengan jelas (Tidak blur, rusak atau tertutup jari/pantulan cahaya)" />
                  </ListItemButton>
                  <ListItemButton sx={{ pl: 0 }}>
                    <ListItemIcon>
                      <FiberManualRecordIcon sx={{ fontSize: '0.5rem' }} />
                    </ListItemIcon>
                    <ListItemText primary="no. KTP masih aktif." />
                  </ListItemButton>
                  <ListItemButton sx={{ pl: 0 }}>
                    <ListItemIcon>
                      <FiberManualRecordIcon sx={{ fontSize: '0.5rem' }} />
                    </ListItemIcon>
                    <ListItemText primary="KTP milik sendiri. Sistem akan memerika kesesuain dengan from yang kamu isi sebelumnya." />
                  </ListItemButton>
                  <ListItemButton sx={{ pl: 0 }}>
                    <ListItemIcon>
                      <FiberManualRecordIcon sx={{ fontSize: '0.5rem' }} />
                    </ListItemIcon>
                    <ListItemText primary="Format file .jpg, .jpeg atau .png" />
                  </ListItemButton>
                </List>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Box component="span" mt={3} sx={{ width: '100%', padding: 48, border: '1px dashed grey' }}>
          <Button variant="outlined">Pilih File</Button>
        </Box>
      </MainCard>
    </Page>
  );
};

Error.getLayout = function getLayout(page) {
  return <Layout variant={LAYOUT.minimal}>{page}</Layout>;
};

export default Error;
