import { Typography } from '@mui/material';
import { Box } from '@mui/system';

const KTPImageUpload = () => (
  <Box
    sx={{
      width: '100%',
      border: '1px dashed #263238'
    }}
  >
    {true ? (
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
          padding: '32px 0'
        }}
      >
        <img src="/assets/images/registration/verifikasi-ktp.png" alt="Foto KTP Yoga" />
        <Typography variant="subtitle1" my={1}>
          Foto KTP Yoga
        </Typography>
        <Typography variant="subtitle3">16 Kb</Typography>
      </Box>
    ) : (
      <Box
        component="span"
        mt={3}
        py={10}
        sx={{
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center'
        }}
      >
        <img src="/assets/images/registration/upload-file.png" alt="Upload" /> 
        <Typography variant="subtitle1" my={3}>
          Cari File atau Drag Langsung ke dalam Box ini
        </Typography>
        <Button variant="outlined">Pilih File</Button>
      </Box>
    )}
  </Box>
);

export default KTPImageUpload;
