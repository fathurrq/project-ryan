import { Grid, List, ListItemButton, ListItemIcon, ListItemText } from '@mui/material';
import MuiTypography from '@mui/material/Typography';

import FiberManualRecordIcon from '@mui/icons-material/FiberManualRecord';
import { Box } from '@mui/system';

const KTPImageGuide = () => (
  <Grid container>
    <Grid item xs={12} sm={4}>
      <img src="/assets/images/registration/verifikasi-ktp.png" alt="Contoh Gambar KTP" /> 
    </Grid>
    <Grid item xs={12} sm={8}>
      <Box>
        <MuiTypography variant="h2" gutterBottom>
          Panduan Foto KTP
        </MuiTypography>

        <List component="div" disablePadding>
          <ListItemButton sx={{ pl: 0 }}>
            <ListItemIcon>
              <FiberManualRecordIcon sx={{ fontSize: '0.5rem' }} />
            </ListItemIcon>
            <ListItemText primary="Foto dan data nomor KTP terbaca dengan jelas (Tidak blur, rusak atau tertutup jari/pantulan cahaya)" />
          </ListItemButton>
          <ListItemButton sx={{ pl: 0 }}>
            <ListItemIcon>
              <FiberManualRecordIcon sx={{ fontSize: '0.5rem' }} />
            </ListItemIcon>
            <ListItemText primary="no. KTP masih aktif." />
          </ListItemButton>
          <ListItemButton sx={{ pl: 0 }}>
            <ListItemIcon>
              <FiberManualRecordIcon sx={{ fontSize: '0.5rem' }} />
            </ListItemIcon>
            <ListItemText primary="KTP milik sendiri. Sistem akan memerika kesesuain dengan from yang kamu isi sebelumnya." />
          </ListItemButton>
          <ListItemButton sx={{ pl: 0 }}>
            <ListItemIcon>
              <FiberManualRecordIcon sx={{ fontSize: '0.5rem' }} />
            </ListItemIcon>
            <ListItemText primary="Format file .jpg, .jpeg atau .png" />
          </ListItemButton>
        </List>
      </Box>
    </Grid>
  </Grid>
);

export default KTPImageGuide;