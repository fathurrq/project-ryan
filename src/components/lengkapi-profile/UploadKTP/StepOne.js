// material-ui
import { Alert, Button, Grid, List, ListItemButton, ListItemIcon, ListItemText, Typography } from '@mui/material';
import MuiTypography from '@mui/material/Typography';

import FiberManualRecordIcon from '@mui/icons-material/FiberManualRecord';

// assets
import MainCard from 'components/ui-component/cards/MainCard';
import { Box } from '@mui/system';
import KTPImageUpload from './FormItems/KTPImageUpload';
import KTPImageGuide from './FormItems/KTPImageGuide';

const StepOne = () => (
  <MainCard title="Upload Foto KTP" sx={{ minHeight: 1200 }}>
    <KTPImageGuide />
    <KTPImageUpload />
    <Grid container mt={2}>
      {true ? <Grid item sx={6} sm={3}>
        <Alert variant="filled" severity="success">
          Dokumen Sudah Sesuai
        </Alert>
      </Grid> : <></>}
    </Grid>
    <Box mt={2} sx={{ display: 'flex', justifyContent: 'end', gap: 2 }}>
      <Button variant="outlined" color="error" sx={{ px: 5 }}>
        Kembali
      </Button>
      <Button variant="contained" disabled sx={{ px: 5 }}>
        Lanjut
      </Button>
    </Box>
  </MainCard>
);

export default StepOne;
