// material-ui
import { useTheme } from '@mui/material/styles';
import { Grid, Stack, Typography } from '@mui/material';


const KenapaCompleted = () => {
  const theme = useTheme();
  const listSX = {
    display: 'flex',
    alignItems: 'center',
    gap: '0.7rem',
    padding: '10px 0',
    fontSize: '1rem',
    color: theme.palette.grey[900],
    svg: { color: theme.palette.secondary.main }
  };

  return (
      <Grid container justifyContent="space-between" alignItems="center" spacing={{ xs: 3, sm: 5, md: 6, lg: 10 }}>
        <Grid item xs={12} md={12}>
          <Grid container spacing={2.5}>
            <Grid item xs={12} sm={12} md={5} lg={5} xl={5}>
              <Stack sx={{ display: 'flex', flexDirection: 'row' }}>
                <Typography variant="h2" sx={{ fontSize: { xs: '1.5rem', sm: '2.125rem' }, mb: 0, mr: 1 }}>
                  Kenapa
                </Typography>
                <Typography variant="h2" sx={{ fontSize: { xs: '1.5rem', sm: '2.125rem' }, mb: 4, color: '#2EABFF' }}>
                  Completed
                </Typography>
              </Stack>
            </Grid>
          </Grid>
         
        </Grid>
      </Grid>
  );
};

export default KenapaCompleted;
