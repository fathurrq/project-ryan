// material-ui
import { useTheme } from '@mui/material/styles';
import { Button, CardMedia, Grid, Stack, Typography } from '@mui/material';

import { IconArrowNarrowRight } from '@tabler/icons';

const DashboardImage = '/assets/images/dashboard.svg';

// ==============================|| LANDING - CUSTOMIZE ||============================== //

const Feature = () => {
  const theme = useTheme();
  const listSX = {
    display: 'flex',
    alignItems: 'center',
    gap: '0.7rem',
    padding: '10px 0',
    fontSize: '1rem',
    color: theme.palette.grey[900],
    svg: { color: theme.palette.secondary.main }
  };

  return (
        <Grid item xs={12}>
          <Grid container spacing={2.5} direction={{ xs: 'column-reverse', md: 'row' }}>
            <Grid item xs={12} md={6}>
              <Grid container spacing={2.5}>
                <Grid item xs={12}>
                  <Typography variant="h2" sx={{ fontSize: { xs: '1.5rem', sm: '2.125rem' }, mb: 0 }}>
                    Fitur Unggulan
                  </Typography>
                  <Typography variant="h2" sx={{ fontSize: { xs: '1.5rem', sm: '2.125rem' }, mb: 4, color: '#2EABFF' }}>
                    Completed
                  </Typography>
                  <Typography
                    variant="h5"
                    sx={{
                      fontSize: { xs: '1rem', sm: '1.5rem' },
                      mb: 2,
                      display: 'flex',
                      flexDirection: 'row',
                      alignItems: 'center',
                      fontWeight: 700
                    }}
                  >
                    Input Orderan Massal <span style={{ width: '20px' }} /> <IconArrowNarrowRight size={24} />
                  </Typography>
                  <Typography
                    variant="subtitle2"
                    color="text.primary"
                    sx={{
                      fontSize: '1rem',
                      zIndex: '99',
                      width: { xs: '100%', md: 'calc(100% - 20%)' },
                      mb: 2
                    }}
                  >
                    Anti ribet untuk kamu yang harus input orderan cukup banyak setiap hari, Gak perlu lagi input data manual satu persatu.
                  </Typography>
                  <Typography
                    variant="h5"
                    sx={{
                      fontSize: { xs: '1rem', sm: '1.5rem' },
                      mb: 2,
                      display: 'flex',
                      flexDirection: 'row',
                      alignItems: 'center',
                      fontWeight: 700
                    }}
                  >
                    Pantau Kendala Pengiriman <span style={{ width: '20px' }} /> <IconArrowNarrowRight size={24} />
                  </Typography>
                  <Typography
                    variant="subtitle2"
                    color="text.primary"
                    sx={{
                      fontSize: '1rem',
                      zIndex: '99',
                      width: { xs: '100%', md: 'calc(100% - 20%)' },
                      mb: 2
                    }}
                  >
                    Kendala pengiriman akan makin mudah dan cepat teratasi karena terhubung langsung dengan ekspedisi terkait
                  </Typography>
                  <Typography
                    variant="h5"
                    sx={{
                      fontSize: { xs: '1rem', sm: '1.5rem' },
                      mb: 2,
                      display: 'flex',
                      flexDirection: 'row',
                      alignItems: 'center',
                      fontWeight: 700
                    }}
                  >
                    Update Resi Otomatis Real-Time <span style={{ width: '20px' }} /> <IconArrowNarrowRight size={24} />
                  </Typography>
                  <Typography
                    variant="subtitle2"
                    color="text.primary"
                    sx={{
                      fontSize: '1rem',
                      zIndex: '99',
                      width: { xs: '100%', md: 'calc(100% - 20%)' },
                      mb: 2
                    }}
                  >
                    Kendala pengiriman akan makin mudah dan cepat teratasi karena terhubung langsung dengan ekspedisi terkait
                  </Typography>
                  <Button variant="outlined" href="#" target="_blank" sx={{ padding: ' 0.5rem 2rem ' }}>
                    Fitur Selengkapnya
                  </Button>
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={12} md={6} sx={{ img: { width: '100%' } }}>
              <Stack sx={{ width: '70%', mx: 'auto' }}>
                <CardMedia component="img" image={DashboardImage} alt="Layer" />
              </Stack>
            </Grid>
          </Grid>
        </Grid>
  );
};

export default Feature;
