// third-party
import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  data: {}
};

const slice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    doLogin: (state, action) => {
      state.data = action.payload;
    }
  }
});

// Reducer
export default slice.reducer;
