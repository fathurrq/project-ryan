const addZeroes = (num) => (num >= 10 ? num : `0${num}`);

export { addZeroes };
