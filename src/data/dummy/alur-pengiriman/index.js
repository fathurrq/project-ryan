const alurPengiriman = [
  {
    id: '1',
    text: 'Masukkan data Order',
    colorTheme: '#11C4F5',
    icon: '/assets/images/landingpage/alur/alur-01.png'
  },
  {
    id: '2',
    text: 'Siapkan Orderan',
    colorTheme: '#5642EF',
    icon: '/assets/images/landingpage/alur/alur-02.png'
  },
  {
    id: '3',
    text: 'Ekspedisi Menjemput & Mengantar Paket',
    colorTheme: '#992DF0',
    icon: '/assets/images/landingpage/alur/alur-03.png'
  },
  {
    id: '4',
    text: 'Barang Diterima',
    colorTheme: '#F546A3',
    icon: '/assets/images/landingpage/alur/alur-04.png'
  },
  {
    id: '5',
    text: 'Dana Dicairkan',
    colorTheme: '#F74662',
    icon: '/assets/images/landingpage/alur/alur-05.png'
  }
];

export { alurPengiriman };
