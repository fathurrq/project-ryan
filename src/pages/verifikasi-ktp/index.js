import Link from 'Link';

// material-ui
import { useTheme, styled } from '@mui/material/styles';
import { Alert, Button, Card, CardContent, CardMedia, Grid, InputLabel, List, ListItemButton, ListItemIcon, ListItemText, TextField, Typography } from '@mui/material';
import MuiTypography from '@mui/material/Typography';

import FiberManualRecordIcon from '@mui/icons-material/FiberManualRecord';

// project imports
import LAYOUT from 'constant';
import Layout from 'layout';
import Page from 'components/ui-component/Page';
import { DASHBOARD_PATH } from 'config';
import AnimateButton from 'components/ui-component/extended/AnimateButton';
import { gridSpacing } from 'store/constant';

// assets
import HomeTwoToneIcon from '@mui/icons-material/HomeTwoTone';
import MainCard from 'components/ui-component/cards/MainCard';
import Image from 'next/image';
import { Box } from '@mui/system';
import StepOne from 'components/lengkapi-profile/UploadKTP/StepOne';

const imagePurple = '/assets/images/maintenance/img-error-purple.svg';

// styles
const CardMediaWrapper = styled('div')({
  maxWidth: 720,
  margin: '0 auto',
  position: 'relative'
});
// ==============================|| ERROR PAGE ||============================== //

const VerifikasiKTP = () => {
  const theme = useTheme();

  return (
    <Page title="Verifikasi KTP">
      <StepOne />
    </Page>
  );
};

Error.getLayout = function getLayout(page) {
  return <Layout variant={LAYOUT.minimal}>{page}</Layout>;
};

export default VerifikasiKTP;
