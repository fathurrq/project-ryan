import Link from 'Link';
// material-ui
import { useTheme } from '@mui/material/styles';
import { Grid, Stack, Typography, useMediaQuery } from '@mui/material';

// project imports
import LAYOUT from 'constant';
import Layout from 'layout';
import Page from 'components/ui-component/Page';

import AuthCardWrapper from 'components/authentication/AuthCardWrapper';

import Image from 'next/image';
import LoginForm from 'components/auth/login/LoginForm';

import { useSelector } from 'react-redux';

const LogoCompleted = '/assets/completed-image/completed-logo.svg';
const LoginIlus = '/assets/completed-image/auth-page/login.png';

const RegisterPage = () => {
  const theme = useTheme();
  const matchDownSM = useMediaQuery(theme.breakpoints.down('md'));
  const data = useSelector((state) => state);
  console.log(data);

  return (
    <Page title="Register">
      <Grid item container justifyContent="space-between" alignItems="center" sx={{ minHeight: '100vh' }}>
        <Grid
          item
          container
          md={6}
          lg={5}
          sx={{ position: 'relative', alignSelf: 'stretch', display: { xs: 'none', md: 'block' } }}
          bgcolor={theme.palette.primary.light}
        >
          <Grid
            item
            container
            justifyContent="flex-start"
            sx={{ display: 'flex', height: '100%', paddingTop: '40px', margin: '0 auto' }}
            alignItems="center"
          >
            <Grid item xs={12}>
              <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                <Image src={LogoCompleted} alt="completed-logo" width={180} height={44} />
              </div>
            </Grid>
            <Grid item xs={12}>
              <div
                style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center', textAlign: 'center' }}
              >
                <Image src={LoginIlus} alt="completed-logo" width={553} height={553} />
                <Typography color={theme.palette.success.dark} variant="h4" fontWeight={600} alignItems="center">
                  Dapatkan Semua Kemudahan Pengiriman Logistik Dalam Satu Platform.
                </Typography>
              </div>
            </Grid>
            <Grid item xs={12}>
              <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }} />
            </Grid>
          </Grid>
        </Grid>
        <Grid item container justifyContent="center" md={6} lg={7} sx={{ my: 3 }}>
          <AuthCardWrapper>
            <Grid container spacing={2} justifyContent="center">
              <Grid item xs={12}>
                <Grid
                  container
                  direction={matchDownSM ? 'column-reverse' : 'row'}
                  alignItems={matchDownSM ? 'center' : 'inherit'}
                  justifyContent={matchDownSM ? 'center' : 'space-between'}
                >
                  <Grid item>
                    <Stack justifyContent={matchDownSM ? 'center' : 'flex-start'} textAlign={matchDownSM ? 'center' : 'inherit'}>
                      <Typography color={theme.palette.grey[10]} gutterBottom variant={matchDownSM ? 'h3' : 'h2'}>
                        Selamat datang Kembali
                      </Typography>
                      <Typography color={theme.palette.grey[500]} variant="body1">
                        Silahkan masuk dan memulai kemudahan dalam mengelola tokomu dalam 1 platform.
                      </Typography>
                    </Stack>
                  </Grid>
                  <Grid item sx={{ mb: { xs: 3, sm: 0 } }}>
                    <Link href="#" aria-label="theme-logo" />
                  </Grid>
                </Grid>
              </Grid>
              <Grid item xs={12}>
                <LoginForm />
              </Grid>

              <Grid item xs={12}>
                <div style={{ display: 'flex', flexDirection: 'row', gap: '4px', alignItems: 'items' }}>
                  <Typography variant="subtitle1">Belum punya Akun?</Typography>
                  <Typography
                    variant="subtitle1"
                    sx={{ textDecoration: 'none' }}
                    component={Link}
                    href="/auth/registration"
                    color={theme.palette.primary[800]}
                  >
                    Daftar Sekarang
                  </Typography>
                </div>
              </Grid>
              <Grid item xs={12}>
                <Grid item container direction="column" alignItems="flex-end" xs={12}>
                  <Typography
                    component={Link}
                    href="/pages/authentication/auth1/register"
                    variant="subtitle1"
                    sx={{ textDecoration: 'none' }}
                  >
                    Don&apos;t have an account?
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          </AuthCardWrapper>
        </Grid>
      </Grid>
    </Page>
  );
};

RegisterPage.getLayout = function getLayout(page) {
  return <Layout variant={LAYOUT.minimal}>{page}</Layout>;
};

export default RegisterPage;
