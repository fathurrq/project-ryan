export const mainAPI = process.env.REACT_APP_API_URL ?? '';
export const jwtPrefix = process.env.COMPLETED_JWT_PREFIX ?? '';
