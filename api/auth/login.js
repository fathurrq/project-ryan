import axios from 'axios';
import { mainAPI } from '../config';

const apiUrl = mainAPI;

export const postLogin = async (data) => {
  const res = await axios.post(apiUrl, data);
  return res.data;
};

export const postRegistration = async (data) => {
  const res = await axios.post(apiUrl, data);
  return res.data;
};
